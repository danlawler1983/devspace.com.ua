import App from "./components/app.svelte";
import L from "leaflet"

import {setUserAgentClassNames} from "./browser/navigator";

const body = document.body;

setUserAgentClassNames(body);

const app = new App({
    target: body,
    props: {}
});

const delta = 5.0;

const mapBounds = [
    [49.5109-delta, 31.6016-delta],
    [49.5109+delta, 31.6016+delta],
];

const map = L.map("map", {zoomSnap: 0.25}).fitBounds(mapBounds);

const tiles = L.tileLayer("https://{s}.tile.osm.org/{z}/{x}/{y}.png", {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
}).addTo(map);

export default app;